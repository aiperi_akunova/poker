import React from 'react';
import './Cards.css'
const Card = props => {
    const suits = {
        'D': 'diams',
        'S': 'spades',
        'H': 'hearts',
        'C': 'clubs',
    };

    const suitSymbol = {
        'D': '♦',
        'S': '♠',
        'H': '♥',
        'C': '♣',
    };
    const suitClass = 'card ' + suits[props.suit]+ ' rank-'+props.rank.toLowerCase();

    return (
        <div className={suitClass}>
            <span className='rank'>{props.rank}</span>
            <span className='suit'>{suitSymbol[props.suit]}</span>
        </div>
    );
};

export default Card;