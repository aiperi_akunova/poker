import './App.css';
import Card from "./Card/Card";
import {Component} from "react";

class  CardDeck{
    constructor() {
      const cardDeck = [];
      const suits = ['D','H','S','C'];
      const ranks = ['2','3','4','5','6','7','8','9','10','J','Q','K','A'];

      for (let i = 0; i < suits.length; i++) {
        for (let j = 0; j < ranks.length; j++) {
        cardDeck.push({suit: suits[i], rank: ranks[j]});
        }
      }
        this.cardDeck = cardDeck;
        console.log(cardDeck)
    }
    getCard=()=>{
        const randomIndex = Math.floor(Math.random()*this.cardDeck.length-1)+1;
        const [card] = this.cardDeck.splice(randomIndex,1);
        return card;
    }
    getCards=(howMany)=>{
        const totalCards = [];
        for (let i = 0; i < howMany; i++) {

            totalCards.push(this.getCard());
        }
        return totalCards;
    }
}

class PokerHand{
    constructor(myCards){
        const fiveRanks = myCards.map(c=>{
            return   c.rank;
        });
        const fiveSuits = myCards.map(c=>{
            return c.suit;
        });

        const myFiveCards = myCards.map(c=>{
            return c.rank + ' '+c.suit;
        })
        // console.log(myFiveCards);
        // console.log(fiveRanks);
        // console.log(fiveSuits);
        this.myCards=myFiveCards;
        this.myRanks= fiveRanks;
        this.mySuits= fiveSuits;
        // console.log(this.myRanks)

    }
    getOutcome=()=>{
        const setOfCards = new Set(this.myRanks);
        console.log(setOfCards.size);
        const differenceInLength = this.myRanks.length - setOfCards.size;
        if(differenceInLength === 1){
            console.log('One pair');
            return 'One pair';
        }
        const count = {};
        this.myRanks.forEach(i=> { count[i] = (count[i]||0) + 1;});
        const objValues = Object.values(count);
        const setOfCValues = new Set(objValues);
        // console.log(setOfCards.size);
        const objSizeDifference = objValues.length - setOfCValues.size;

       if(objValues.includes(3)){
           console.log('Three of a kind');
           return 'Three of a kind'
       }else if(objSizeDifference === 1){
           console.log('Two pairs');
           return 'Two pairs';
       }
        const countSuits = {};
        this.mySuits.forEach(i=> { countSuits[i] = (countSuits[i]||0) + 1;});
        if(Object.values(countSuits).includes(5)){
            console.log('Flush');
            return 'Flush'
        }
    }
}

const newCardDeck = new CardDeck();
class App extends Component {
    state = {
            card: newCardDeck.getCards(5),
    }
    newCardDeckFromBtn = ()=>{
        const newCardDecks = new CardDeck();
        this.setState({card:newCardDecks.getCards(5)});
    }

    render() {
        const myPokerHand = new PokerHand(this.state.card);
        return (
            <div className="App cards-container">

                <div className='playingCards cards-box'>
                    {this.state.card.map((card,i)=>{
                        return <Card key={i} suit = {card.suit} rank={card.rank}/>
                    })}
                </div>
                <button onClick={this.newCardDeckFromBtn} className='new-card-deck-btn'>New Card Deck</button>
                <h1>{myPokerHand.getOutcome()}</h1>
            </div>
        );
    }
}

export default App;
